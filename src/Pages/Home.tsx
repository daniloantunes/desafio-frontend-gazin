import React from "react";

import FilmsService from "../Services/films-service";
const filmsService = new FilmsService();

function Home() {
  const [filmesPopulares, setFilmesPopulares] = React.useState<any[]>([]);
  const [proximosLancamentos, setProximosLancamentos] = React.useState<any[]>([]);
  const [atoresPopulares, setAtoresPopulares] = React.useState<any[]>([]);

  React.useEffect(() => {
    listaFilmesPopulares();
    listaProximosLancamentos();
    //listaAtoresPopulares();
  }, []);

  const listaFilmesPopulares = () => {
    filmsService.filmesPopulares().then((r: any) => {
      setFilmesPopulares(r);
    });
  };

  const listaProximosLancamentos = () => {
    filmsService.proximosLancamentos().then((r: any) => {
      setProximosLancamentos(r);
    });
  };

  const listaAtoresPopulares = () => {
    filmsService.atoresPopulares().then((r: any) => {
      setAtoresPopulares(r);
    });
  };

  return (
    <div id='home'>
      <h2>Home</h2>

      <h3>Banner</h3>
      <div>{filmesPopulares[0].original_title}</div>

      <h3>Filmes Populares</h3>
      {filmesPopulares.map((p: any, i: number) => {
        if (i)
          return <div key={p.id}>{i} {p.original_title}</div>
      })}
      <hr />

      <h3>Próximos Lançamentos</h3>
      {proximosLancamentos.map((l: any) => 
        <div key={l.id}>{l.original_title}</div>
      )}
      <hr />

      <h3>Atores Populares</h3>
      {atoresPopulares.map((a: any) => 
        <div key={a.id}>{a.original_title}</div>
      )}
      <hr />
    </div>
  );
}

export default Home;
