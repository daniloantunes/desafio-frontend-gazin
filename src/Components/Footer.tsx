function Footer() {
  return (
    <footer>
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-12">
            GazinFilms © 2022
          </div>
          <div className="col-sm-12">
            Desenvolvido por @DaniloAntunes
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
  