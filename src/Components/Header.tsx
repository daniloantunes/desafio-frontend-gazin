import React from 'react';
import {Link} from 'react-router-dom';

function Header() {
  const [keyword, setKeyword] = React.useState<string>(() => '');

  return (
    <header className="App-header">
      <div className='container-fluid'>
        <div className='row'>
          <div className='col-sm-3'>
            <Link to={'/'}>
              <h1>GazinFilms</h1>
            </Link>
          </div>
          <div className='col-sm-6 pull-right search-container'>
            <input placeholder='pesquisa...' className='form-control search-input' value={keyword} onChange={(e) => setKeyword(e.target.value)} />
            <Link to={{pathname: "/search", search: `?k=${keyword}`}}>
              <i className="bi bi-search search-submit"></i>
            </Link>
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;
