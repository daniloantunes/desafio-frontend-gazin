import React from 'react';
import './App.css';

import {Routes, Route} from 'react-router-dom';

import Header from './Components/Header';
import Footer from './Components/Footer';

import Home from './Pages/Home';
import Search from './Pages/Search';
import MoveDetail from './Pages/MoveDetail';
import ActorDetail from './Pages/ActorDetail';

function App() {
  return (
    <div className="App">
      <Header />
      <section id='secao-principal'>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/search" element={<Search />} />
          <Route path="/move-detail" element={<MoveDetail />} />
          <Route path="/actor-detail" element={<ActorDetail />} />
        </Routes>
      </section>
      <Footer />
    </div>
  );
}

export default App;
