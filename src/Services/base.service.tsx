import axios from 'axios';

export default class BaseService {
  private axios: any;
  public headers = {};

  public url = {
    tmdb: (path: string, p: number | null = null) => {
      return `https://api.themoviedb.org/3/${path}?api_key=686d2d29bfdd702761b1ff13af5fc787&language=pt-BR&page=${p || 1}`
    }
  }

  async http(metodo: string, path: string, dados: any = null) {
    this.axios = axios.create({
      headers: {
        'Content-Type': 'application/json',
      },
    });
    try {
      const response = await this.axios({
        method: metodo,
        url: path,
        data: dados,
      });
      return this.handle(response);
    } catch (error) {
      return this.handleError(error);
    } finally {
      this.finally();
    }
  }

  handle(resposta: any) {
    if (resposta.data.results) {
      return resposta.data.results;
    }
    return this.handleError(resposta);
  }

  handleError(error: any) {
    let e: any;
    if (error.response) {
      if (error.response.data.errors) {
        e = error.response.data.errors[0];
      } else {
        e = error.response.data;
      }
    } else {
      e = error.data;
    }
    return Promise.reject(e);
  }

  finally() {}

  // Requisições de consulta
  get(path: string): Promise<Response> {
    return this.http('get', path);
  }

  // Requisições de Inserção
  post(path: string, dados: any = null): Promise<Response> {
    return this.http('post', path, dados);
  }

  // Requisições de Atualização
  put(path: string, dados: any = null): Promise<Response> {
    return this.http('put', path, dados);
  }

  // Requisições de Remoção
  delete(path: string, dados: any = null): Promise<Response> {
    return this.http('delete', path, dados);
  }
}
