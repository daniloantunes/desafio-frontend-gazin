import BaseService from './base.service';

export default class FilmsService extends BaseService {

  filmesPopulares(p: number | null = null): Promise<Response> {
    const url = this.url.tmdb('movie/popular', p);
    return this.get(url);
  }

  proximosLancamentos(p: number | null = null): Promise<Response> {
    const url = this.url.tmdb('movie/upcoming', p);
    return this.get(url);
  }

  atoresPopulares(): Promise<Response> {
    return this.get('/fichas');
  }

  search(): Promise<Response> {
    return this.get('/fichas');
  }

  filmDetails(): Promise<Response> {
    return this.get('/fichas');
  }

  actorDetails(): Promise<Response> {
    return this.get('/fichas');
  }

}
